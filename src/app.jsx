// react
import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'

// custom components
import Header from './components/header.jsx'

// pages
import About from './components/pages/aboutComponent.jsx'
import Home from './components/pages/homeComponent.jsx'
import Bye from './components/pages/byeComponent.jsx'

// css files
import './index.css';

class App extends React.Component {
   constructor() {
      super();
   }
   render() {
      return (
         <div>
            <Header/>

              <Route path="/home" component={Home} />
              <Route path="/about" component={About} />
              <Route path="/bye" component={Bye} />


         </div>
      );
   }
}
export default App;
