import React from 'react'

import axios from 'axios'

/*
heirarchy
 - APIHandler shall be parent of services such as user and feed.


*/

class APIHandler {
  constructor() {

    this.http = axios
  }

  // create general functions to later override by the child classes
  getFeed() {}

  getUsers() {}
}

export default APIHandler
