import React from 'react'

import APIHandler from '../APIHandler.js'

class FeedService {
  constructor() {
    this.feed = [
      {
        title: 'Hej',
        description: 'hellloo',
        postedBy: 'postedByMe'
      },
      {
        title: 'Hej',
        description: 'hellloo',
        postedBy: 'postedByMe'
      },
      {
        title: 'Hej',
        description: 'hellloo',
        postedBy: 'postedByMe'
      },
    ]
  }

  getFeed() {
    return this.feed
  }
}

export default FeedService
