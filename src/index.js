import React from 'react';
import ReactDOM from 'react-dom';
import App from './app.jsx';

import {
  BrowserRouter,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'

ReactDOM.render(<BrowserRouter><App/></BrowserRouter>, document.getElementById('app'));
