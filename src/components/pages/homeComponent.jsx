import React from 'react';

import TableRow from '../tableRow.jsx'
import Card from  '../card.jsx'

import UserService from '../../service/users/userService.js'
import FeedService from '../../service/feed/feedService.js'


const Home = () => {
    this.userService = new UserService()
    this.users = this.userService.getUsers()

    this.feedService = new FeedService()
    this.feed = this.feedService.getFeed()

    console.log(this.feed);

    return (
      <div className="container">
        <div className="sub-container-70 sub-container">
          <div className="container-title">
            <h3>Recent feed</h3>
          </div>
          { this.feed.map((cardData, i) => <Card key = {i}
            data = {cardData} />) }
        </div>

        <div className="sub-container-30 sub-container">
          <div className="container-title">
            <h3>Users</h3>
          </div>
          <table>
             <tbody>
                { this.users.data.map((person, i) => <TableRow key = {i}
                   data = {person} />) }
             </tbody>
          </table>
        </div>
      </div>
    );
}
export default Home;
