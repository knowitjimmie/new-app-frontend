import React from 'react'

class Card extends React.Component {

  render() {
    return (
      <div className="card">
        <h1>{ this.props.data.title }</h1>
        <h3>{ this.props.data.description }</h3>
        <p>{ this.props.data.postedBy }</p>
      </div>
    )
  }
}

export default Card
