import React from 'react';

import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom'

class Header extends React.Component {
constructor() {
  super()

  // mocked header properties.
  this.headerProps = [
    {
      text: 'Home',
      href: '/home'
    },
    {
      text: 'about',
      href: '/about'
    },
    {
      text: 'bye',
      href: '/bye'
    }
  ]
}

   render() {
      return (
         <div>
            <h1>Facebook</h1>
            <nav>
              <ul>
                  { this.headerProps && this.headerProps.map(item => <li> <a><Link to={item.href}>{item.text}</Link></a> </li>)}
              </ul>

            </nav>
         </div>
      );
   }
}

export default Header
