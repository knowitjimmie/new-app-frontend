import React from 'react';

/*
A typical content-parent, as this one, shall conform to React.Compontents

Children shall conform to Content.

example of children to apply:
Header, info content, contact content, Jumbotron(Bootstrap design basically.)

*/

private class Content extends React.Components {
  constructor(props) {
    super();

    // global var to store the HTML in.
    // sub-classes will assign its content to this.
    this.content = null
  }

  // TODO:
  // add somekind of general functions that all types of content are able to use.

  removeContentComponent() {
    // Not sure if this already exists in React. A method to remove an object in the DOM.
    // This function would be great if the data is dynamic and a box/content shall be removed if a certain condition is met.
    this.content.remove()
  }

  render() {
    return this.content
  }
}

export default Content
