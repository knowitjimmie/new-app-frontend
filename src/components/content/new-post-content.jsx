import React from 'React'
import Content from './content.jsx'

class NewPostContent extends Content {
  constructor() {
    // shall init its parent to access general functions, vars and design patterns.
    super();
  }

  function submitPost(e) {
    e.preventDefault()

  }

  render() {
    // this is a simple jumbotron alike.

    // What I'm, trying to accomplish is for the superclass to render and not the children.

    this.content = (
      <div className="content newPostContent">
        <div className="newPostContent-content">
          <input type="text" placeholder="What's on your mind?"></input>
          <button type="button" onClick={submitPost}></button>
        </div>
      </div>
    )
    super.render();
  }
}

export default HeaderContent
