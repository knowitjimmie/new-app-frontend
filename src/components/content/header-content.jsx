import React from 'React'
import Content from './content.jsx'

class HeaderContent extends Content {
  constructor() {
    // shall init its parent to access general functions, vars and design patterns.
    super();
  }

  render() {
    // this is a simple jumbotron alike.

    // What I'm, trying to accomplish is for the superclass to render and not the children.

    this.content = (
      <div className="content headerContent">
        <div className="headerContent-content">
          <h3></h3>
        </div>
      </div>
    )
    super.render();
  }
}

export default HeaderContent
