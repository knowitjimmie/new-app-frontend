import React from 'react';

class TableRow extends React.Component {

  // shall be a class in heirarchy of tableRow and not a function within it.
  renderUsers() {
    return (
       <tr>
          <td>{this.props.data.id}</td>
          <td>{this.props.data.name}</td>
          <td>{this.props.data.age}</td>
       </tr>
    );
  }

   render() {
     return this.renderUsers()
   }
}

export default TableRow
