import React from 'react';

import Content from 'content/content.jsx'

/*
Args:
Size {Number} - 30, 50, 70, 100
content {Content} - ..
*/
class Container extends React.components {

  constructor(props) {
    this.size = props.size
    this.content = props.content

    if(this.size >= 100) {
      // shall be a parent container.
      this.setClassName = "container"
    } else {
      // if the container shouldnt be a full-width container, aka a binder
      // set its child width.
      this.setClassName = "sub-container-" + this.size.toString()
    }
  }

  render() {
    return (
      <div className={this.setClassName}
        // Content shall be a component container, that has a heirarchy.
        // for example:
        /*
        Content should be a type of abstraction, where it has children that conforms to it.

        In a scenario where the argument for "containers" take a "content" abstraction class.
        Where it shouldnt care wether its a sub-class or subsub-class, or whatever.
        The design of the page shall conform to different types of content design's.
        */
        {content}
      </div>
    )
  }
}
